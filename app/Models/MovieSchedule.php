<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MovieSchedule extends Model
{
    use HasFactory;

    protected $fillable = [ 'movie_id', 'schedule_id' ];

    public function movie() {
        return $this->belongsTo(Movies::class);
    }
    
    public function schedule() {
        return $this->belongsTo(Schedule::class);
    }
}
