<?php

namespace Database\Seeders;

use App\Models\Movies;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;



class MoviesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        define("SCHEDULE", '120 min');
        define("IMG", 'https://static.cinepolis.com/img/peliculas/39361/1/1/39361.jpg');

        $data = [
            [
                'title' => 'Thor: Love And Thunder',
                'slug' => Str::slug('Thor: Love And Thunder', '-'),
                'image' => IMG,
                'duration' => SCHEDULE,
                'classification' => 'B',
                'publication_date' => "20/05/2025",
                'subtitled' => false,
                'status' => true,
                'created_at' => Carbon::now()
            ],
            [
                'title' => 'Tren Bala',
                'slug' => Str::slug('Tren Bala', '-'),
                'image' => IMG,
                'duration' => SCHEDULE,
                'classification' => 'B15',
                'publication_date' => "14/03/2025",
                'subtitled' => false,
                'status' => true,
                'created_at' => Carbon::now()
            ],
            [
                'title' => 'Minions: Nace Un Villano',
                'slug' => Str::slug('Minions: Nace Un Villano', '-'),
                'image' => IMG,
                'duration' => SCHEDULE,
                'classification' => 'A',
                'publication_date' => "12/09/2025",
                'subtitled' => false,
                'status' => true,
                'created_at' => Carbon::now()
            ]
        ];
        
        Movies::insert($data);
    }
}
