<?php

namespace App\Http\Controllers;

use App\Models\Movies;
use App\Http\Requests\StoreMoviesRequest;
use App\Http\Requests\UpdateMoviesRequest;
use App\Models\MovieSchedule;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Movies::with('schedule')->get();
        return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMoviesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMoviesRequest $request)
    {
        $request->validate([
            'title'          => 'required',
            'duration'       => 'required',
            'classification' => 'required',
        ], [
            'title.required'            => 'Debe agregar un título',
            'duration.required'         => 'Campo requerido',
            'classification.required'   => 'Campo requerido',
        ]);

        $schedule = Movies::create([
            'title'             => $request->title,
            'slug'              => Str::slug($request->title),
            // 'image'             => $request->image,
            'image'             => 'https://static.cinepolis.com/img/peliculas/39361/1/1/39361.jpg',
            'duration'          => $request->duration,
            'classification'    => $request->classification,
            'publication_date'  => $request->publication,
            'subtitled'         => $request->subtitled,
            'status'            => true,
        ]);

        return $schedule ? response()->json($schedule, 201) : response()->json('Error al agregar el horario', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function show(Movies $movies)
    {
        //
    }

    public function assingSchedules(){
        $responseData = request()->all();
        $movie_id = collect($responseData)->map(function ($item){
            return Movies::findOrFail($item['movie_id']);
        });
        if ($movie_id) {
            $movies_schedules = json_decode(request()->getContent(), true);
            $response = MovieSchedule::insert($movies_schedules);
    
            return $response ? response()->json($response, 200) : response()->json('Error al agregar la asignación de horarios', 400);
        }
        response()->json('Error', 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMoviesRequest  $request
     * @param  \App\Models\Movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMoviesRequest $request, Movies $movies)
    {
        $movies->update(
            [
                'title' => request('title'), 
                'slug' => Str::slug(request('title')),
                'image' => request('image'),
                'duration' => request('duration'),
                'classification' => request('classification'),
                'publication_date' => request('publication'),
                'subtitled' => request('subtitled'),
                'status' => request('status')
            ]
        );
        return $movies ? response()->json($movies, 200) : response()->json('error', 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movies $movies)
    {
        $deleted = $movies->delete();

        return $deleted ? response()->json('Eliminado correctamente', 200) : response()->json('error', 400);
    }
}
