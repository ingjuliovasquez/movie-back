<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movies extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'slug', 'image', 'duration', 'classification', 'publication_date', 'subtitled', 'status'];

    public function schedule() {
        return $this->hasMany(MovieSchedule::class, 'movie_id');
    }
}
