<?php

namespace Database\Seeders;

use App\Models\Schedule;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            [
                'schedule' => '14:00',
                'slug' => Str::slug('14:00'),
                'status' => true,
                'created_at' => Carbon::now()
            ],
            [
                'schedule' => '16:00',
                'slug' => Str::slug('16:00'),
                'status' => true,
                'created_at' => Carbon::now()
            ],
            [
                'schedule' => '18:00',
                'slug' => Str::slug('18:00'),
                'status' => true,
                'created_at' => Carbon::now()
            ],
            [
                'schedule' => '20:00',
                'slug' => Str::slug('20:00'),
                'status' => true,
                'created_at' => Carbon::now()
            ],
            [
                'schedule' => '22:00',
                'slug' => Str::slug('22:00'),
                'status' => true,
                'created_at' => Carbon::now()
            ],
        ];

        Schedule::insert($data);
    }
}
