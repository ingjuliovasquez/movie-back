<?php

namespace App\Http\Controllers;

use App\Models\Schedule;
use App\Http\Requests\StoreScheduleRequest;
use App\Http\Requests\UpdateScheduleRequest;
use Illuminate\Support\Str;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Schedule::with('movie')->get();
        return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreScheduleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreScheduleRequest $request)
    {
        $request->validate([
            'schedule' => 'required',
        ], [
            'schedule.required' => 'Debe agregar un horario'
        ]);

        $schedule = Schedule::create([
            'schedule' => $request->schedule,
            'slug' => Str::slug($request->schedule),
            'status' => $request->status,

        ]);

        return $schedule ? response()->json($schedule, 201) : response()->json('Error al agregar el horario', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateScheduleRequest  $request
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateScheduleRequest $request, Schedule $schedule)
    {
        $schedule->update(['schedule' => request('schedule'), 'status' => request('status')]);
        return $schedule ? response()->json($schedule, 200) : response()->json('error', 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        $deleted = $schedule->delete();

        return $deleted ? response()->json('Eliminado correctamente', 200) : response()->json('error', 400);
    }
}
