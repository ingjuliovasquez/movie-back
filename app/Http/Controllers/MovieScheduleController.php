<?php

namespace App\Http\Controllers;

use App\Models\MovieSchedule;
use App\Http\Requests\StoreMovieScheduleRequest;
use App\Http\Requests\UpdateMovieScheduleRequest;

class MovieScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMovieScheduleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMovieScheduleRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MovieSchedule  $movieSchedule
     * @return \Illuminate\Http\Response
     */
    public function show(MovieSchedule $movieSchedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MovieSchedule  $movieSchedule
     * @return \Illuminate\Http\Response
     */
    public function edit(MovieSchedule $movieSchedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMovieScheduleRequest  $request
     * @param  \App\Models\MovieSchedule  $movieSchedule
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMovieScheduleRequest $request, MovieSchedule $movieSchedule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MovieSchedule  $movieSchedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(MovieSchedule $movieSchedule)
    {
        //
    }
}
