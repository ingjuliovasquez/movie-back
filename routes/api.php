<?php

use App\Http\Controllers\MoviesController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->group(function () {
    Route::resource('schedules', ScheduleController::class);
    Route::resource('movies', MoviesController::class);
    Route::post('assing-schedules', [MoviesController::class, 'assingSchedules']);
});

Route::post('login', [LoginController::class, 'login']);
