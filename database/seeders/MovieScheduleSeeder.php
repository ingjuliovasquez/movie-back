<?php

namespace Database\Seeders;

use App\Models\MovieSchedule;
use Illuminate\Database\Seeder;

class MovieScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MovieSchedule::create([
            'schedule_id' => 1,
            'movie_id' => 1,
        ]);
        MovieSchedule::create([
            'schedule_id' => 2,
            'movie_id' => 2,
        ]);
        MovieSchedule::create([
            'schedule_id' => 3,
            'movie_id' => 3,
        ]);
        MovieSchedule::create([
            'schedule_id' => 1,
            'movie_id' => 3,
        ]);
        MovieSchedule::create([
            'schedule_id' => 2,
            'movie_id' => 3,
        ]);
    }
}
