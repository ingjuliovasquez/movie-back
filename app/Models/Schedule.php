<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;

    protected $fillable = ['schedule', 'slug', 'status'];

    public function movie() {
        return $this->hasMany(MovieSchedule::class);
    }
}
